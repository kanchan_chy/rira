package com.bscheme.rira;

import android.content.Context;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by ASUS on 10/16/2015.
 */
public class UIHandler {

  //  public static boolean connectionStatus;
    public static Context context;
    public static SweetAlertDialog mDialog=null;

    public UIHandler(Context context)
    {
        this.context=context;
    }


    public static void showProgress()
    {
        mDialog = new SweetAlertDialog(context,SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
        mDialog.setTitleText("Connecting with robot...");
        mDialog.setCancelable(false);
        mDialog.show();
    }

    public static void cancelProgress()
    {
        mDialog.dismiss();
    }

}
