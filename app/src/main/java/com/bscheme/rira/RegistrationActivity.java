package com.bscheme.rira;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationActivity extends AppCompatActivity {

    EditText edtName,edtMail,edtPassword,edtCustomerId;
    Button btnSubmit;
    SweetAlertDialog mDialog;

    String mail="",name="",pass="",customer="";
    int success;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);
        edtName=(EditText)findViewById(R.id.edtName);
        edtMail=(EditText)findViewById(R.id.edtMail);
        edtPassword=(EditText)findViewById(R.id.edtPassword);
        edtCustomerId=(EditText)findViewById(R.id.edtCustomerId);
        btnSubmit=(Button)findViewById(R.id.btnSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mail="";
                name="";
                pass="";
                customer="";
                mail=edtMail.getText().toString();
                name=edtName.getText().toString();
                pass=edtPassword.getText().toString();
                customer=edtCustomerId.getText().toString();
                if(name.equals("")||mail.equals("")||pass.equals("")||customer.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Please fill all options", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(isValidEmail(mail))
                    {
                        mDialog = new SweetAlertDialog(RegistrationActivity.this,SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
                        mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primaryColorDark));
                        mDialog.setTitleText("");
                        mDialog.setCancelable(false);
                        mDialog.show();

                        registerUser();
                    }
                    else Toast.makeText(getApplicationContext(),"Invalid Email address",Toast.LENGTH_SHORT).show();
                }

              //  new AsyncTaskRunnerGCM().execute();

            }
        });


    }


    @Override
    public void onBackPressed() {
        Intent in=new Intent(RegistrationActivity.this,MainActivity.class);
        startActivity(in);
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
        finish();
    }







    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }





    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }




    public void registerUser()
    {
        String url=Urls.REGISTER_URL;

        StringRequest mRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mDialog.dismiss();
                    JSONObject json=new JSONObject(response);
                    success=json.getInt("success");
                    if(success==1)
                    {
                        Toast.makeText(getApplicationContext(),"Registration successful",Toast.LENGTH_LONG).show();
                        Intent in=new Intent(RegistrationActivity.this,MainActivity.class);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                        finish();
                    }
                    else Toast.makeText(getApplicationContext(),"Registration failed",Toast.LENGTH_LONG).show();
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Registration failed",Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("name", name);
                params.put("email" , mail);
                params.put("password", pass);
                params.put("customer_id", customer);
            //    params.put("device_id", deviceId);
                return params;

            }
        };
        Volley.newRequestQueue(this).add(mRequest);

    }






}
