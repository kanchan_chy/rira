package com.bscheme.rira;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    EditText edtMail,edtPassword;
    Button btnSignIn,btnSignUp;
    CheckBox checkBox;
    SweetAlertDialog mDialog;

    SharedPreferences prefSignin;
    SharedPreferences.Editor signinEditor;

    String mail="",pass="";
    int success;
    String regId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtMail=(EditText)findViewById(R.id.edtMail);
        edtPassword=(EditText)findViewById(R.id.edtPassword);
        btnSignIn=(Button)findViewById(R.id.btnSignIn);
        btnSignUp=(Button)findViewById(R.id.btnSignUp);
        checkBox=(CheckBox)findViewById(R.id.checkBox);

        prefSignin=getSharedPreferences("Signin",MODE_PRIVATE);
        signinEditor=prefSignin.edit();

        mail = prefSignin.getString("mail","");
        pass=prefSignin.getString("pass","");

        if(!mail.equals("")&&!pass.equals(""))
        {
            checkBox.setChecked(true);
            edtMail.setText(mail);
            edtPassword.setText(pass);
        }

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mail=edtMail.getText().toString();
                pass=edtPassword.getText().toString();
                if(mail.equals("")||pass.equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Please fill all options",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(isValidEmail(mail))
                    {
                        mDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
                        mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primaryColorDark));
                        mDialog.setTitleText("");
                        mDialog.setCancelable(false);
                        mDialog.show();
                        makeLogin();
                    }
                    else Toast.makeText(getApplicationContext(),"Invalid Email address",Toast.LENGTH_SHORT).show();
                }
               // sendGcmMessage();
            }
        });


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, RegistrationActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                finish();
            }
        });


    }





    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }



    @Override
    public void onBackPressed() {
        this.finish();
        System.exit(1);
    }






    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }




    public void successLogin(String customerId)
    {
        signinEditor.putString("customer",customerId);
        if(checkBox.isChecked())
        {
            signinEditor.putString("mail",mail);
            signinEditor.putString("pass",pass);
            signinEditor.commit();
        }
        else
        {
            signinEditor.putString("mail","");
            signinEditor.putString("pass","");
            signinEditor.commit();
        }
        Intent in=new Intent(MainActivity.this,OptionActivity.class);
        startActivity(in);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        finish();
    }




    public void makeLogin()
    {
        String url=Urls.LOGIN_URL;

        StringRequest mRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mDialog.dismiss();
                    JSONObject json=new JSONObject(response);
                    success=json.getInt("success");
                    if(success==1)
                    {
                        String customerId=json.getString("customer_id");
                        successLogin(customerId);
                    }
                    else Toast.makeText(getApplicationContext(),"User not found",Toast.LENGTH_LONG).show();
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Toast.makeText(getApplicationContext(),"User not found",Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("email" , mail);
                params.put("password", pass);
                return params;

            }
        };
        Volley.newRequestQueue(this).add(mRequest);
    }



}
