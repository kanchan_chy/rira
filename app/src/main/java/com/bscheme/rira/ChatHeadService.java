package com.bscheme.rira;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dreamers.util.Content;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ASUS on 10/7/2015.
 */
public class ChatHeadService extends Service implements View.OnTouchListener{

    private WindowManager windowManager;
    WindowManager.LayoutParams[] params=new WindowManager.LayoutParams[12];
    private ImageView[] chatHeads=new ImageView[12];

    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;

   // private String[] options={"option_a","option_b","option_c","option_d","option_e","option_f","option_g","option_h","option_connect","option_exit"};

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    SharedPreferences prefChat;
    SharedPreferences.Editor editorChat;

    SharedPreferences prefGCM;
    SharedPreferences.Editor gcmEDitor;
    String regId,regId2="";
    String customerId;
    int success;

    boolean connectionStatus,movable,stoppable;
    int width,height;

    String PROJECT_NUMBER =   "732660555741";
    boolean gcmIdProblem = false;
    GoogleCloudMessaging gcm;
    boolean registrationSuccess=false;

    GCMMessageSender sender;
    ArrayList<String> arrRegId;


    @Override public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override public void onCreate() {
        super.onCreate();

        pref=getSharedPreferences("Switch", MODE_PRIVATE);
        editor=pref.edit();
        connectionStatus=false;
        movable=true;

        prefChat=getSharedPreferences("Chat_Head", MODE_PRIVATE);
        editorChat=prefChat.edit();

        SharedPreferences prefSigin=getSharedPreferences("Signin", MODE_PRIVATE);
        customerId=prefSigin.getString("customer","");

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        for(int i=0;i<9;i++)
        {
            chatHeads[i] = new ImageView(this);
            chatHeads[i].setScaleType(ImageView.ScaleType.FIT_XY);
        }

        chatHeads[0].setImageResource(R.drawable.button_a);
        chatHeads[1].setImageResource(R.drawable.button_b);
       // chatHeads[2].setImageResource(R.drawable.button_c);
       // chatHeads[3].setImageResource(R.drawable.button_d);
        chatHeads[2].setImageResource(R.drawable.button_e);
        chatHeads[3].setImageResource(R.drawable.button_f);
        chatHeads[4].setImageResource(R.drawable.button_g);
        chatHeads[5].setImageResource(R.drawable.button_h);
        chatHeads[6].setImageResource(R.drawable.connect);
        chatHeads[7].setImageResource(R.drawable.exit);
        chatHeads[8].setImageResource(R.drawable.move_on);

        for(int i=0;i<9;i++)
        {
     /*       params[i] = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);    */

            if(i==6)
            {
                params[i] = new WindowManager.LayoutParams(
                        120,
                        50,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);

                params[i].gravity = Gravity.TOP | Gravity.LEFT;
            }
            else
            {
                params[i] = new WindowManager.LayoutParams(
                        75,
                        75,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);

                params[i].gravity = Gravity.TOP | Gravity.LEFT;
            }
        }

        params[0].x = prefChat.getInt("x"+0,0);
        params[0].y = prefChat.getInt("y"+0,(height/2+40));

        params[1].x = prefChat.getInt("x"+1,0);
        params[1].y = prefChat.getInt("y"+1,(height/2+140));

   /*     params[2].x = 0;
        params[2].y = height/2+60;

        params[3].x = 0;
        params[3].y = height/2+120;  */

        params[2].x = prefChat.getInt("x"+2,(width/2-25));
        params[2].y = prefChat.getInt("y"+2,(height/2-40));;

        params[3].x = prefChat.getInt("x"+3,(width/2+65));
        params[3].y = prefChat.getInt("y"+3,(height/2+50));

        params[4].x = prefChat.getInt("x"+4,(width/2-25));
        params[4].y = prefChat.getInt("y"+4,(height/2+140));

        params[5].x = prefChat.getInt("x"+5,(width/2-115));
        params[5].y = prefChat.getInt("y"+5,(height/2+50));

        params[6].x = prefChat.getInt("x"+6,(width/2+60));
        params[6].y = prefChat.getInt("y"+6,(height/2-65));

        params[7].x = prefChat.getInt("x"+7,(width-85));
        params[7].y = prefChat.getInt("y"+7,100);

        params[8].x = prefChat.getInt("x"+8,(width-85));
        params[8].y = prefChat.getInt("y"+8,185);

        for(int i=0;i<9;i++)
        {
            windowManager.addView(chatHeads[i], params[i]);
            chatHeads[i].setOnTouchListener(this);
        }


    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean tempFirst=intent.getExtras().getBoolean("first_time");
        if(tempFirst==true)
        {
            connectionStatus=false;
        }
        else
        {
            String tempString=intent.getExtras().getString("connection");
            if(tempString.equals("yes"))
            {
                if(connectionStatus==true) Toast.makeText(getApplicationContext(),"Disconnecting failed",Toast.LENGTH_SHORT).show();
                else Toast.makeText(getApplicationContext(),"Connected",Toast.LENGTH_SHORT).show();
                connectionStatus=true;
                chatHeads[6].setImageResource(R.drawable.disconnect);
                windowManager.updateViewLayout(chatHeads[6], params[6]);
            }
            else
            {
                if(connectionStatus==false) Toast.makeText(getApplicationContext(),"Connection failed",Toast.LENGTH_SHORT).show();
                else Toast.makeText(getApplicationContext(),"Disconnected",Toast.LENGTH_SHORT).show();
                connectionStatus=false;
                chatHeads[6].setImageResource(R.drawable.connect);
                windowManager.updateViewLayout(chatHeads[6], params[6]);
            }
        }
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(int i=0;i<9;i++)
        {
            if (chatHeads[i] != null)
            {
                editorChat.putInt("x"+i,params[i].x);
                editorChat.putInt("y"+i,params[i].y);
                windowManager.removeView(chatHeads[i]);
            }
        }
        editorChat.commit();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int pos=-1;
        for(int i=0;i<9;i++)
        {
            if((ImageView)view==chatHeads[i])
            {
                pos=i;
                break;
            }
        }
        if(pos!=-1)
        {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = params[pos].x;
                    initialY = params[pos].y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();
                    if(!movable)
                    {
                        if(pos!=6&&pos!=7&&pos!=8)
                        {
                            stoppable=true;
                            Toast.makeText(getApplicationContext(),"Character will be sent",Toast.LENGTH_SHORT).show();
                           // optionSelected(pos);
                        }
                    }
                    return true;
                case MotionEvent.ACTION_UP:
                    if( (Math.abs(initialTouchX - event.getRawX())<5) && (Math.abs(initialTouchY - event.getRawY())<5) )
                    {
                        if(pos==6||pos==7||pos==8)
                        {
                            optionSelected(pos);
                        }
                        else
                        {
                            if(!movable)
                            {
                                stoppable=false;
                                Toast.makeText(getApplicationContext(),"Stop will be sent",Toast.LENGTH_SHORT).show();
                                //sendGcmMessage("button","7");
                            }
                        }

                       // Toast.makeText(getApplicationContext(),"Up",Toast.LENGTH_SHORT).show();
                        //optionSelected(pos);
                    }
                    //  else Toast.makeText(getApplicationContext(),"Moved",Toast.LENGTH_SHORT).show();
                    return true;
                case MotionEvent.ACTION_MOVE:
                    if(movable)
                    {
                        params[pos].x = initialX + (int) (event.getRawX() - initialTouchX);
                        params[pos].y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHeads[pos], params[pos]);
                    }
                    else
                    {
                        if(pos!=6&&pos!=7&&pos!=8)
                        {
                            if( (Math.abs(initialTouchX - event.getRawX())>8) && (Math.abs(initialTouchY - event.getRawY())>8) )
                            {
                                if(stoppable)
                                {
                                    stoppable=false;
                                    Toast.makeText(getApplicationContext(),"Stop will be sent",Toast.LENGTH_SHORT).show();
                                    //sendGcmMessage("button","7");
                                }
                            }
                        }
                    }
                    return true;
            }
        }

        return false;
    }




    public void optionSelected(final int pos)
    {
       // Toast.makeText(getApplicationContext(), option+" Clicked", Toast.LENGTH_SHORT).show();
        if(pos==8)
        {
            if(movable)
            {
                movable=false;
                chatHeads[pos].setImageResource(R.drawable.move_off);
                windowManager.updateViewLayout(chatHeads[pos], params[pos]);
            }
            else
            {
                movable=true;
                chatHeads[pos].setImageResource(R.drawable.move_on);
                windowManager.updateViewLayout(chatHeads[pos], params[pos]);
            }
        }
        else if(pos==7)
        {
            //Option Exit selected
            // service closed
            editor.putBoolean("checked", false);
            editor.commit();
            stopSelf();
        }
        else if(pos==6)
        {
            //Option Connect or Disconnect selected
            if(connectionStatus)
            {
                if(connectionStatus)
                {
                    sendGcmMessage("button","*");
                }
                else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();

                Toast.makeText(getApplicationContext(),"Disconnected", Toast.LENGTH_SHORT).show();
                connectionStatus=false;
                chatHeads[6].setImageResource(R.drawable.connect);
                windowManager.updateViewLayout(chatHeads[pos], params[pos]);
                //Disconnected..... Do stuff here...
            }
            else
            {
                //Connected..... Do stuff here...

                prefGCM=getSharedPreferences("GCM", MODE_PRIVATE);
                regId=pref.getString("reg_id","");

                Toast.makeText(getApplicationContext(),"Wait for connecting with robot controller", Toast.LENGTH_SHORT).show();
                // UIHandler.connectionStatus=true;
                //chatHeads[8].setImageResource(R.drawable.disconnect);
              //  windowManager.updateViewLayout(chatHeads[pos], params[pos]);

                if(regId.equals(""))
                {
                    new AsyncTaskRunnerGCM().execute();
                }
                else
                {
                    if(regId2.equals(""))
                    {
                        success=0;
                        getRegId();
                    }
                    else
                    {
                        success=0;
                        connectOtherEnd();
                    }
                }
          /*      Handler mHandler=new Handler();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(UIHandler.connectionStatus==false)
                        {
                            chatHeads[8].setImageResource(R.drawable.connect);
                            windowManager.updateViewLayout(chatHeads[pos], params[pos]);
                        }
                    }
                },3500);  */
            }
        }
        else if(pos==0)
        {
            //Option A selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","%");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
        else if(pos==1)
        {
            //Option B selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","^");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
    /*    else if(pos==2)
        {
            //Option C selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","clock");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
        else if(pos==3)
        {
            //Option D selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","anticlock");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }   */
        else if(pos==2)
        {
            //Option E selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","!");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
        else if(pos==3)
        {
            //Option F selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","$");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
        else if(pos==4)
        {
            //Option G selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","@");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
        else if(pos==5)
        {
            //Option H selected.... Do stuff here....
            if(connectionStatus)
            {
                sendGcmMessage("button","#");
            }
            else Toast.makeText(getApplicationContext(),"Not Connected",Toast.LENGTH_SHORT).show();
        }
    }



    private void sendGcmMessage(String title, String value)
    {
        Content content=   sender.createContent(title, value, arrRegId);
        sender.send(content);
    }




    private void connectOtherEnd()
    {
        if(!regId2.equals(""))
        {
            sender=new GCMMessageSender();
            arrRegId=new ArrayList<String>();
            arrRegId.add(regId2);
            sendGcmMessage("reg_id",regId);
        }
    }




    public void getRegId()
    {
        String url=Urls.GET_REG_ID_URL;

        StringRequest mRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json=new JSONObject(response);
                    success=json.getInt("success");
                    if(success==1)
                    {
                        regId2=json.getString("reg_id");
                        connectOtherEnd();
                    }
                    else
                    {
                      //  mDialog.dismiss();
                     //   UIHandler.cancelProgress();
                        Toast.makeText(getApplicationContext(),"No robot found with this customer id",Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e)
                {
                  //  mDialog.dismiss();
                  //  UIHandler.cancelProgress();
                    Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  mDialog.dismiss();
               // UIHandler.cancelProgress();
                Toast.makeText(getApplicationContext(),"Network problem",Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("customer_id" , customerId);
                return params;

            }
        };
        Volley.newRequestQueue(this).add(mRequest);
    }



    class AsyncTaskRunnerGCM extends AsyncTask<String, String, String> {

        int suc = 5;
        String error="";
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                // --------------Getting Gcm ID
                try {

                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regId = gcm.register(PROJECT_NUMBER);


                } catch (Exception ex) {
                    error=ex+"";
                    gcmIdProblem = true;
                    Log.d("ERROR IN GETTING ID", error);
                }

                // ______________________________________

                if (!gcmIdProblem) {

                    registrationSuccess = true;

                } else {
                    registrationSuccess = false;
                }

            } catch (Exception e) {
                error=error+e;
            }

            return null;

        }

        protected void onPostExecute(String string) {

            if (registrationSuccess == true) {
                gcmEDitor=prefGCM.edit();
                gcmEDitor.putString("reg_id", regId);
                gcmEDitor.commit();

                if(regId2.equals(""))
                {
                    success=0;
                    getRegId();
                }
                else
                {
                    success=0;
                    connectOtherEnd();
                }
            } else {
              //  mDialog.dismiss();
               // UIHandler.cancelProgress();
                Toast.makeText(getApplicationContext(), "Failed to register with GCM", Toast.LENGTH_LONG).show();
            }

        }


    }






}

