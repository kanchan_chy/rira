package com.bscheme.rira;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class OptionActivity extends AppCompatActivity {

    SwitchCompat switchCompat;
    LinearLayout linearSignout;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String customerId;
    boolean checked,destroying;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.option_layout);
        switchCompat=(SwitchCompat)findViewById(R.id.mSwitch);
        linearSignout=(LinearLayout)findViewById(R.id.linearSignout);

        destroying=false;

        pref=getSharedPreferences("Switch", MODE_PRIVATE);
        editor=pref.edit();

        checked=pref.getBoolean("checked", false);

        switchCompat.setChecked(checked);

        linearSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SweetAlertDialog dialog = new SweetAlertDialog(OptionActivity.this, SweetAlertDialog.WARNING_TYPE);
                dialog.setTitleText("Are you sure?");
                dialog.setConfirmText("Sign out");
                dialog.setCancelText("Cancel");
                dialog.setCancelable(true);
                dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        SharedPreferences prefSigin = getSharedPreferences("Signin", MODE_PRIVATE);
                        SharedPreferences.Editor signinEditor = prefSigin.edit();
                        signinEditor.putBoolean("signed", false);
                        signinEditor.commit();
                        sweetAlertDialog.dismiss();
                        destroying = true;
                        Intent in = new Intent(OptionActivity.this, MainActivity.class);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                        finish();
                    }
                });
                dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });

                dialog.show();


            }
        });


        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean chk) {
                if (appInstalledOrNot("com.skype.raider")) {
                    if (chk) {
                        PackageManager packageManager = getPackageManager();
                        Intent skypeIntent=packageManager.getLaunchIntentForPackage("com.skype.raider");
                        skypeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(skypeIntent);

                        Toast.makeText(getApplicationContext(), "Please wait for the Skype to be opened", Toast.LENGTH_SHORT).show();

                        Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            public void run() {
                                editor.putBoolean("checked", true);
                                editor.commit();
                                Intent in = new Intent(OptionActivity.this, ChatHeadService.class);
                                in.putExtra("first_time",true);
                                startService(in);

                                SharedPreferences prefSigin = getSharedPreferences("Signin", MODE_PRIVATE);
                                SharedPreferences.Editor signinEditor = prefSigin.edit();
                                signinEditor.putBoolean("signed", false);
                                signinEditor.commit();
                                destroying = true;
                                finish();
                                System.exit(1);
                            }
                        }, 2500);
                    } else {
                        editor.putBoolean("checked", false);
                        editor.commit();
                        Intent in = new Intent(OptionActivity.this, ChatHeadService.class);
                        stopService(in);
                    }
                } else
                    Toast.makeText(getApplicationContext(), "Skype is not installed in your device", Toast.LENGTH_LONG).show();

          /*      if (chk) {
                    editor.putBoolean("checked", true);
                    editor.commit();
                    Intent in = new Intent(OptionActivity.this, ChatHeadService.class);
                    startService(in);

                    Toast.makeText(getApplicationContext(), "Please wait for the Skype to be opened", Toast.LENGTH_SHORT).show();

                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        public void run() {
                            SharedPreferences prefSigin = getSharedPreferences("Signin", MODE_PRIVATE);
                            SharedPreferences.Editor signinEditor = prefSigin.edit();
                            signinEditor.putBoolean("signed", false);
                            signinEditor.commit();
                            destroying = true;
                            finish();
                            System.exit(1);
                        }
                    }, 2000);

                } else {
                    editor.putBoolean("checked", false);
                    editor.commit();
                    Intent in = new Intent(OptionActivity.this, ChatHeadService.class);
                    stopService(in);
                }   */

            }
        });

    }


    @Override
    public void onBackPressed() {

        SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        dialog.setTitleText("Exit without Signing out?");
        dialog.setConfirmText("Exit");
        dialog.setCancelText("Cancel");
        dialog.setCancelable(false);
        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreferences prefSigin=getSharedPreferences("Signin",MODE_PRIVATE);
                SharedPreferences.Editor signinEditor=prefSigin.edit();
                signinEditor.putBoolean("signed",true);
                signinEditor.commit();
                sweetAlertDialog.dismiss();
                destroying=true;
                finish();
                System.exit(1);
            }
        });
        dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        });

        dialog.show();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(destroying==false)
        {
            SharedPreferences prefSigin=getSharedPreferences("Signin",MODE_PRIVATE);
            SharedPreferences.Editor signinEditor=prefSigin.edit();
            signinEditor.putBoolean("signed",true);
            signinEditor.commit();
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }



}
