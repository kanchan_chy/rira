package com.bscheme.rira;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * Created by ASUS on 10/6/2015.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash_layout);
        new TimeSpender().execute();
    }


    @Override
    public void onBackPressed() {

    }


    class TimeSpender extends AsyncTask<String,String,String>
    {
        @Override
        protected String doInBackground(String... strings) {
            try {
                Thread.sleep(1500);
            }
            catch (Exception e) {}
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            SharedPreferences prefSigin=getSharedPreferences("Signin", MODE_PRIVATE);
            boolean signed=prefSigin.getBoolean("signed", false);
            Intent in;
            if(signed) in=new Intent(SplashActivity.this,OptionActivity.class);
            else in=new Intent(SplashActivity.this,MainActivity.class);
            startActivity(in);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            finish();
        }
    }


}
